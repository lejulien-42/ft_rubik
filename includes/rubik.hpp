/* ************************************************************************** */
/*                                                                            */
/*                                                                      / )   */
/*   rubik.hpp                                          (\__/)         ( (    */
/*                                                      )    (          ) )   */
/*   By: lejulien <leo.julien.42@gmail.com>           ={      }=       / /    */
/*                                                      )     `-------/ /     */
/*   Created: 2023/01/22 20:24:25 by lejulien          (               /      */
/*   Updated: 2023/01/26 01:00:41 by lejulien           \              |      */
/*                                                                            */
/* ************************************************************************** */

#include "pix.hpp"
#include <iostream>
#include <vector>
#include <cstring>
#include <fcntl.h>
#include <fstream>
#include <cstdint>

#define IMAGE_WIDTH 216
#define IMAGE_HEIGHT 165

#pragma once

class Rubik {
		private:
				std::vector<char>	_front;
				std::vector<char>	_up;
				std::vector<char>	_left;
				std::vector<char>	_right;
				std::vector<char>	_down;
				std::vector<char>	_back;
				pix::Image				_data;
				int _step = 0;
				void							_draw_face(std::vector<char> *, pix::pos);
				void							_rotate_face(std::vector<char> *);
				void							_execute_command(const char rot, const char mod);
				void							_execute_command(const char rot);
				void							_execute_double_command(const char &rot);
				void							_execute_prime_command(const char rot);
				const 						std::vector<char> _getFace(const char face);
				char							_getColor(const char face, unsigned int pos);
				void							_cross();
				uint32_t					_checkNeigbors(const char face);
				int 							_checkCenterBased(const std::vector<char> same_face, const std::vector<char> outer);
				std::vector<char>	_getFaceEdges(const char face);
				void _saveCubeAs(const char *filepath, const char *title, int step);
				std::string				_create_path();
		public:
				Rubik(const char *algorythm);

				// Utilities
				void solve(void);
				void resetCube();
				// Instructions
				void F();
				void FP();
				void TWO_F();
				void U();
				void UP();
				void TWO_U();
				void L();
				void LP();
				void TWO_L();
				void R();
				void RP();
				void TWO_R();
				void D();
				void DP();
				void TWO_D();
				void B();
				void BP();
				void TWO_B();

				//enum
				
				typedef enum {
					TL,
					TM,
					TR,
					ML,
					M,
					MR,
					BL,
					BM,
					BR
				}			positions; // face positions

};


/*     Cube memory representation
 *
 *         _up
 *
 *		     W W W
 *         W W W
 *         W W W
 *
 *  _Left  _front _right _back
 *
 *  R R R  B B B  O O O  G G G
 *  R R R  B B B  O O O  G G G
 *  R R R  B B B  O O O  G G G
 *
 *  		   _down
 *
 *         Y Y Y
 *         Y Y Y
 *         Y Y Y
 */


