/* ************************************************************************** */
/*                                                                            */
/*                                                                      / )   */
/*   main.cc                                            (\__/)         ( (    */
/*                                                      )    (          ) )   */
/*   By: lejulien <leo.julien.42@gmail.com>           ={      }=       / /    */
/*                                                      )     `-------/ /     */
/*   Created: 2023/01/22 20:16:51 by lejulien          (               /      */
/*   Updated: 2023/01/26 01:01:07 by lejulien           \              |      */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/rubik.hpp"
#include <iostream>

// TODO : add last applied instruction to the cube

int	main(int ac, char **av) {
	// check if there is an algorythm provided
	if (ac != 2) {
		std::cout << "usage : ./rubik \"ALGORYTHM\"" << std::endl;
	}

	std::cout << "sending this algo to the cube " << av[1] << std::endl;
	Rubik r(av[1]);
	r.resetCube();
	return 0;
}
