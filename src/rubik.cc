/* ************************************************************************** */
/*                                                                            */
/*                                                                      / )   */
/*   rubik.cc                                           (\__/)         ( (    */
/*                                                      )    (          ) )   */
/*   By: lejulien <leo.julien.42@gmail.com>           ={      }=       / /    */
/*                                                      )     `-------/ /     */
/*   Created: 2023/01/22 20:30:21 by lejulien          (               /      */
/*   Updated: 2023/01/26 01:25:46 by lejulien           \              |      */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/rubik.hpp"
#include <stdlib.h>

Rubik::Rubik(const char *algo) :	_front(std::vector<char>(9, 'B')),
									_up(std::vector<char>(9, 'W')),
									_left(std::vector<char>(9, 'R')),
									_right(std::vector<char>(9, 'O')),
									_down(std::vector<char>(9, 'Y')),
									_back(std::vector<char>(9, 'G')),
									_data(pix::Image(IMAGE_WIDTH, IMAGE_HEIGHT)){
	// remove previous output
	system("rm ./out/*");

	// implement scramble cube based on algo
	char last_input = '0';
	char mod = '0';
	int algo_length = strlen(algo);
	for (size_t i = 0; i < algo_length + 1; i++) {
		switch(algo[i]) {
			case 'F':
				last_input = algo[i];
				break;
			case 'U':
				last_input = algo[i];
				break;
			case 'D':
				last_input = algo[i];
				break;
			case 'L':
				last_input = algo[i];
				break;
			case 'R':
				last_input = algo[i];
				break;
			case  'B':
				last_input = algo[i];
				break;
			case '\'':
				mod = algo[i];
				break;
			case '2':
				mod = algo[i];
				break;
			case ' ':
				_execute_command(last_input, mod);
				last_input = '0';
				mod = '0';
				break;
			case '\0':
				_execute_command(last_input, mod);
				last_input = '0';
				mod = '0';
				break;

		}
	}

}

void Rubik::_execute_command(const char rot) {
	switch (rot) {
			case 'F':
				F();
				_saveCubeAs(_create_path().c_str(), "F", _step);
				break;
			case 'U':
				U();
				_saveCubeAs(_create_path().c_str(), "U", _step);
				break;
			case 'D':
				D();
				_saveCubeAs(_create_path().c_str(), "D", _step);
				break;
			case 'L':
				L();
				_saveCubeAs(_create_path().c_str(), "L", _step);
				break;
			case 'R':
				R();
				_saveCubeAs(_create_path().c_str(), "R", _step);
				break;
			case  'B':
				B();
				_saveCubeAs(_create_path().c_str(), "B", _step);
				break;
	}
}

void Rubik::_execute_prime_command(const char rot) {
	switch (rot) {
			case 'F':
				FP();
				_saveCubeAs(_create_path().c_str(), "FP", _step);
				break;
			case 'U':
				UP();
				_saveCubeAs(_create_path().c_str(), "UP", _step);
				break;
			case 'D':
				DP();
				_saveCubeAs(_create_path().c_str(), "DP", _step);
				break;
			case 'L':
				LP();
				_saveCubeAs(_create_path().c_str(), "LP", _step);
				break;
			case 'R':
				RP();
				_saveCubeAs(_create_path().c_str(), "RP", _step);
				break;
			case  'B':
				BP();
				_saveCubeAs(_create_path().c_str(), "BP", _step);
				break;
	}
}

void	Rubik::_execute_double_command(const char &rot) {
	switch (rot) {
			case 'F':
				TWO_F();
				_saveCubeAs(_create_path().c_str(), "2F", _step);
				break;
			case 'U':
				TWO_U();
				_saveCubeAs(_create_path().c_str(), "2U", _step);
				break;
			case 'D':
				TWO_D();
				_saveCubeAs(_create_path().c_str(), "2D", _step);
				break;
			case 'L':
				TWO_L();
				_saveCubeAs(_create_path().c_str(), "2L", _step);
				break;
			case 'R':
				TWO_R();
				_saveCubeAs(_create_path().c_str(), "2R", _step);
				break;
			case  'B':
				TWO_B();
				_saveCubeAs(_create_path().c_str(), "2B", _step);
				break;
	}
}


void Rubik::_execute_command(const char rot, const char mod) {
	switch(mod) {
		case '\'' :
			_execute_prime_command(rot);
			break;
		case '2' :
			_execute_double_command(rot);
			break;
		case '0' :
			_execute_command(rot);
			break;
	}
}

/*
 *
 * Commun cases
 *
 * allready set
 * set but not well oriented15
 * in middle oriented
 * im no
 * ot o
 * ot no
 *
 * priorities :
 * 	1 - if connected place it
 * 	2 - if during mov of no a o is created, connect it
 * 	3 - connect no
 *
 */
int Rubik::_checkCenterBased(const std::vector<char> same_face, const std::vector<char> outer) {
	return -1;
}

std::vector<char>	Rubik::_getFaceEdges(const char face) {
	return {
		_getColor('U', TM),
		_getColor('U', MR),
		_getColor('U', BM),
		_getColor('U', ML)
	};
}

uint32_t	Rubik::_checkNeigbors(const char face) {
	uint32_t ret = 0;
	std::map<char, uint32_t> neighbors = {
		{
			'U', 
			_checkCenterBased(
					_getFaceEdges('U'),
					{_getColor('B', TM), _getColor('R', TM), _getColor('F', TM), _getColor('L', TM)}
			)
		},
		{
			'D',
			_checkCenterBased(
					_getFaceEdges('D'),
					{_getColor('F', BM), _getColor('R', BM), _getColor('B', BM), _getColor('L', BM)}
			)
		}
	};
	
	return ret;
}

/*
 * Cross Algo
 *
 * 1 - detect each center neighbors and choose the most efficent one
 * 			if none choose normal orientation
 *
 * 2 - Try to place edges but check if there is a shorter placing at each
 * 			moves
 *
 */

void Rubik::_cross() {
	// Determine which faces to start with based on neighbors	
	
	
	// execute simple mid pieces placing
}

/*
 *
 * How to solve the cube in less than 50 moves ?
 *
 * -> Try the Frid... Method (The one that I know)
 * -> Try new algorythms like OLL PLL
 * -> Try shorter ones
 *
 *  Applying Frid. Method for now
 */

void Rubik::solve() {
	_cross();
}


void	Rubik::resetCube() {
	std::fill(_front.begin(), _front.end(), 'B');
	std::fill(_up.begin(), _up.end(), 'W');
	std::fill(_left.begin(), _left.end(), 'R');
	std::fill(_right.begin(), _right.end(), 'O');
	std::fill(_down.begin(), _down.end(), 'Y');
	std::fill(_back.begin(), _back.end(), 'G');
}

void	Rubik::_draw_face(std::vector<char> *face, pix::pos pos) {
	for (int y = 0; y < 3; y++) {
		for (int x = 0; x < 3; x++){
			pix::pixel tmp = {0, 0, 0};
			switch ((*face)[x + y * 3]) {
				case 'B':
					tmp = {0, 0, 255};
					break;
				case 'W':
					tmp = {255, 255, 255};
					break;
				case 'R':
					tmp = {255, 0, 0};
					break;
				case 'O':
					tmp = {255, 127, 0};
					break;
				case 'Y':
					tmp = {255, 255, 0};
					break;
				case 'G':
					tmp = {0, 255, 0};
					break;
				default:
					break;
			}
			_data.drawRect({ pos.x + x * 16, pos.y + y * 16}, {16, 16}, {0, 0, 0}); // outline
			_data.drawRect({ pos.x + 2 + x * 16, pos.y + 2 + y * 16}, {12, 12}, tmp); // iner color
		}
	}
}

void	Rubik::_saveCubeAs(const char *path, const char *title, int step) {
	_data.drawRect({0, 0}, {216, 165}, {20, 20, 20});
	std::string tmp("rm ");
	tmp.append(path);
	system(tmp.c_str());
	std::string name = std::to_string(step - 1).append(": ");
	name.append(title);
	// render faces
	_draw_face(&_up   , {56, 0});
	_data.print("U"   , {77, 48}, {0, 255, 255});
	_draw_face(&_left , {0, 56});
	_data.print("L"   , {21, 104}, {0, 255, 255});
	_draw_face(&_down , {56, 110});
	_data.print("D"   , {77, 158}, {0, 255, 255});
	_draw_face(&_front, {56, 56});
	_data.print("F"   , {77, 104}, {0, 255, 255});
	_draw_face(&_right, {112, 56});
	_data.print("R"   , {131, 104}, {0, 255, 255});
	_draw_face(&_back , {168, 56});
	_data.print("B"   , {189, 104}, {0, 255, 255});
	
	_data.print(name, {0, 25}, {0, 255, 255});
	// Save Image
	_data.saveAs(path);
}

const std::vector<char> Rubik::_getFace(const char face) {
	switch (face) {
		case 'U':
			return (_up);
		case 'D':
			return (_down);
		case 'L':
			return (_left);
		case 'R':
			return (_right);
		case 'F':
			return (_front);
		case 'B':
			return (_back);
	}
	return {};
}

/*	getColor
 *  _________
 * /---------\
 * | 0  1  2 |
 * | 3  4  5 |
 * | 6  7  8 |
 * \_________/
 *
 *  input: which face an it's position
 *
 *	return: selected face color
 */

char	Rubik::_getColor(const char face, unsigned int pos) {
	std::vector<char> temp = _getFace(face);
	return temp[pos];
}


// Rotations

void	swap_colors(char *s, char *d) {
	char tmp = *s;
	*s = *d;
	*d = tmp;
}

void	Rubik::_rotate_face(std::vector<char> *side) {
	// Corners
	swap_colors(&(*side)[8], &(*side)[6]);
	swap_colors(&(*side)[2], &(*side)[8]);
	swap_colors(&(*side)[2], &(*side)[0]);
	// Mids
	swap_colors(&(*side)[7], &(*side)[5]);
	swap_colors(&(*side)[1], &(*side)[5]);
	swap_colors(&(*side)[3], &(*side)[1]);
}

// Front Rotations

void	Rubik::F() {
// Front
	_rotate_face(&_front);	
// Outside
	// Right Corners
	swap_colors(&_up[8], &_right[6]);
	swap_colors(&_up[8], &_left[2]);
	swap_colors(&_left[2], &_down[0]);
	// Left Corners
	swap_colors(&_up[6], &_right[0]);
	swap_colors(&_up[6], &_left[8]);
	swap_colors(&_left[8], &_down[2]);
	// Outer Mids
	swap_colors(&_right[3], &_up[7]);
	swap_colors(&_up[7], &_left[5]);
	swap_colors(&_left[5], &_down[1]);
}

std::string Rubik::_create_path() {
	std::string fpath("./out/");
	fpath.append(std::to_string(_step));
	fpath.append(".ppm");
	_step++;
	return fpath;
}

void	Rubik::FP() {
	F();
	F();
	F();
}

void	Rubik::TWO_F() {
	F();
	F();
}

// Left Rotations

void	Rubik::L() {
// Front
	_rotate_face(&_left);
// Outside
	// Right Corners
	swap_colors(&_front[6], &_down[6]);
	swap_colors(&_front[6], &_up[6]);
	swap_colors(&_up[6], &_back[2]);
	// Left Corners
	swap_colors(&_front[0], &_down[0]);
	swap_colors(&_front[0], &_up[0]);
	swap_colors(&_up[0], &_back[8]);
	// Outer Mids
	swap_colors(&_back[5], &_up[3]);
	swap_colors(&_down[3], &_back[5]);
	swap_colors(&_down[3], &_front[3]);
}



void	Rubik::LP() {
	L();
	L();
	L();
}

void	Rubik::TWO_L() {
	L();
	L();
}

// Up Rotations

void	Rubik::U() {
// Front
	_rotate_face(&_up);
// Outside
	// Right Corners
	swap_colors(&_front[2], &_left[2]);	
	swap_colors(&_front[2], &_right[2]);	
	swap_colors(&_right[2], &_back[2]);	
	// Left Corners
	swap_colors(&_front[0], &_left[0]);	
	swap_colors(&_front[0], &_right[0]);	
	swap_colors(&_right[0], &_back[0]);	
	// Outer Mids
	swap_colors(&_front[1], &_left[1]);	
	swap_colors(&_front[1], &_right[1]);	
	swap_colors(&_right[1], &_back[1]);
}

void	Rubik::UP() {
	U();
	U();
	U();
}

void	Rubik::TWO_U() {
	U();
	U();
}

// Down Rotations

void	Rubik::D() {
// Front
	_rotate_face(&_down);
// Outside
	// Right Corners
	swap_colors(&_front[8], &_right[8]);
	swap_colors(&_front[8], &_left[8]);
	swap_colors(&_back[8], &_left[8]);
	// Left Corners
	swap_colors(&_front[6], &_right[6]);
	swap_colors(&_front[6], &_left[6]);
	swap_colors(&_back[6], &_left[6]);
	// Outer Mids
	swap_colors(&_front[7], &_right[7]);
	swap_colors(&_front[7], &_left[7]);
	swap_colors(&_back[7], &_left[7]);
}

void	Rubik::DP() {
	D();
	D();
	D();
}

void	Rubik::TWO_D() {
	D();
	D();
}

// Right Rotations

void	Rubik::R() {
// Front
	_rotate_face(&_right);
// Outside
	// Right Corners
	swap_colors(&_front[8], &_up[8]);
	swap_colors(&_front[8], &_down[8]);
	swap_colors(&_back[0], &_down[8]);
	// Left Corners
	swap_colors(&_front[2], &_up[2]);
	swap_colors(&_front[2], &_down[2]);
	swap_colors(&_back[6], &_down[2]);
	// Outside Mids
	swap_colors(&_front[5], &_up[5]);
	swap_colors(&_front[5], &_down[5]);
	swap_colors(&_back[3], &_down[5]);
} 

void	Rubik::RP() {
	R();
	R();
	R();
}

void	Rubik::TWO_R() {
	R();
	R();
}

// Back Rotations

void	Rubik::B() {
// Front
	_rotate_face(&_back);
// Outside
	// Right Corners
	swap_colors(&_up[2], &_left[0]);
	swap_colors(&_up[2], &_right[8]);
	swap_colors(&_down[6], &_right[8]);
	// Left Corners
	swap_colors(&_up[0], &_left[6]);
	swap_colors(&_up[0], &_right[2]);
	swap_colors(&_down[8], &_right[2]);
	// Outer Mids
	swap_colors(&_up[1], &_left[3]);
	swap_colors(&_up[1], &_right[5]);
	swap_colors(&_down[7], &_right[5]);
}

void	Rubik::BP() {
	B();
	B();
	B();
}

void	Rubik::TWO_B() {
	B();
	B();
}

